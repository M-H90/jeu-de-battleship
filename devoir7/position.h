using namespace std;

class Position
{
public:
	Position();
	Position(int, int);

	int getLigne()const;
	int getColonne()const;
	Position voisinDroite()const;
	Position voisinGauche()const;
	Position voisinHaut()const;
	Position voisinBas()const;

	bool operator == (Position)const;

	bool operator < (Position)const;


private:

	int _ligne;
	int _colonne;

};

const Position INVALIDE = { -1, -1 };




