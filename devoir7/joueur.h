//
//  joueur.h
//  D7-BatailleNavale
//
//  Created by Marie-Flavie Auclair-Fortier on 18-03-21.
//  Copyright © 2018 Marie-Flavie Auclair-Fortier. All rights reserved.
//

#ifndef JOUEUR_H
#define JOUEUR_H

#include "grille.h"

class Joueur
{
private:
    Grille m_tirs;
    Grille m_bateaux;
    Grille m_tirsEnnemi;

    /*
     Description: Valide si un bateau entre dans la grille à partir d'une position
     param[E] i_pos : position a verifier
                i_taille : taille du bateau
                i_orientation : orientation du bateau (HORIZONTAL ou non)
     pre i_pos doit etre a l'interieur des bornes de la grille
        i_taille est entre 2 et 5
     retour booléen
     post vrai si le bateau entre dans la grille et il n'y a pas de collision
        faux sinon
     precision la méthode est valide avec les assertions
    */

    bool validerPositionBateau(const Position &, int, bool) const;

    void placerBateaux();

public:
    /*
     Description: constructeur par defaut
    */

    Joueur();
    
    /*
     Description: affiche le tableau des tirs
     post affichage de la grille des tirs avec entete et no. des lignes, '0' pour un tir rate, '*' pour un tir reussi
    */

    void afficherTirs() const;

 /*
     Description: affiche le tableau des tirs de l'ennemi
     post affichage de la grille des bateaux avec entete et no. des lignes, no. des bateaux a chaque emplacement et '*' pour un tir réussi
    */

   void afficherBateaux() const;

    /*
     Description: affecte le contenu de la case des tirs a touché
     param[E] i_pos : position du tir
     pre i_pos doit etre a l'interieur des bornes de la grille
     post la grille de tirs a la position contient la constante TOUCHE
    */

    void toucher(const Position&);
 
    /*
     Description: affecte le contenu de la case des tirs a raté
     param[E] i_pos : position du tir
     pre i_pos doit etre a l'interieur des bornes de la grille
     post la grille de tirs a la position contient la constante TIR_RATE
    */

    void rater(const Position&);

    /*
     Description: valide si un tir a deja ete effectue a une position donnee
     param[E] i_pos : position a verifier
     pre i_pos doit etre a l'interieur des bornes de la grille
     retour booleen
     post vrai si aucun tir n'a deja ete effectue (contenu de la grille est EAU)
        faux sinon
    */

    bool validerTir(const Position&) const;

    /*
     Description: valide si l'attaque de l'opposant a touché quelquechose
     param[E] i_pos : position a verifier
     pre i_pos doit etre a l'interieur des bornes de la grille
     param[S] o_bCoule : bouléen, indique si le bateau est coulé
     retour booléen
     post la fonction retourne vrai si le bateau est touché, faux sinon
            le paremètre en sortie est vrai si le bateau est coulé, faux sinon
    */

    bool validerAttaqueOpposant(const Position&, bool&);

    /*
     Description: valide si le joueur a perdu
     retour booléen
     post vrai si le joueur a perdu (si toutes les cases sont soit à EAU ou TOUCHE)
        faux sinon
    */

    bool validerSiPerdu() const;

    /*
     Description: valide si le bateau est coulé à partir d'un touche
     param[E] i_pos : position a verifier
     pre i_pos doit etre a l'interieur des bornes de la grille
        le contenu de la grille de tirs à i_pos devrait être TOUCHE
     retour booléen
     post retourne vrai si toutes les cases d'un même bateau ont été touchées
      faux sinon
    */

    bool validerSiCoule(const Position&) const;
};

#endif
