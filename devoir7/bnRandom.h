/*
* fichier : bnRandom.h
* auteur : Vincent Ducharme
* modifié par : Marie-Flavie Auclair-Fortier - 15 octobre
* résumé : Ce fichier contient une fonction de génération de valeur aléatoire
*/

#ifndef BNRANDOM_H
#define BNRANDOM_H

#include <random>
#include <chrono>   // Pour les fonctions de temps
#include <cassert>

/*
* description : génère une valeur aléatoire entière entre deux bornes
* entrées :
   i_min valeur minimale que peut prendre la valeur
   i_max valeur max que peut prendre la valeur
* sorties : aucune
* retour : valeur aléatoire produite
* pré : i_min < i_max
* post : le retour est entre i_min et i_max
* précisions : la fonction est robuste si les assertions sont actives
*/

static int genereValeur(int i_min, int i_max)
{
    assert(i_min<i_max);
    
    unsigned int seed {(unsigned int) std::chrono::system_clock::now().time_since_epoch().count()};
    
    seed = 15; // à enlever si on veux du vrai aléatoire
    
    static std::mt19937 generateur(seed);
    
    uint32_t valeur = generateur();
    
    double res = ((double(valeur)/generateur.max())*(i_max-i_min)) + i_min;
    
    int result = floor(res + 0.5);
    
    assert (result>=i_min);
    assert (result<=i_max);
    
    return result;
}

#endif /* bnRandom_h */
