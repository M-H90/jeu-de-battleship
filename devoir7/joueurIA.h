#include "joueur.h"
#include "constantes.h"
#include <vector>

using namespace std;



class JoueurIA :public Joueur
{
public:
	JoueurIA();

	void afficherTirs() const;
	void afficherBateaux() const;
	void rater(const Position&);
	bool validerTir(const Position&) const;
	bool validerAttaqueOpposant(const Position&, bool&);
	bool validerSiPerdu() const;
	bool validerSiCoule(const Position&) const;


	Position genererAttaque();
	void toucher(const Position& i_pos, bool touche);

	bool isValid(const Position& i_pos);

private:

	Grille m_tirs;
	Grille m_bateaux;
	Grille m_tirsEnnemi;
	vector<Position> dernPosValide;
};