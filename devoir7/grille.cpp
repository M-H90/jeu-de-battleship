/*
* fichier : grille.cpp
* auteur.e : Vincent Ducharme
* date : 2016
* modifications :
* 01/11/2019 Marie-Flavie Auclair-Fortier : refactoring du code
* description : Ce fichier contient une grille pour le jeu de bataille navale
*/

#include "grille.h"

#include <assert.h>

/*
 Description: constructeur par défaut
 post tous les éléments de la grille sont initialisés à EAU
*/

Grille::Grille()
{
    for (int i = 0; i < NB_LIGNES; ++i)
    {
        for (int j = 0; j < NB_COLONNES; ++j)
        {
            m_cases[i][j] = Case::EAU;
        }
    }
}

/*
 Description: accesseur en écriture du contenu de la grille par une position
 param[E] i_pos : une position
 pre i_pos doit être à l'intérieur de la grille
 retour une case de la grille
 post une référence non constante sur la case pour qu'elle puisse être modifiée
 precision la méthode est valide avec les assertions
*/

Grille::Case& Grille::operator[](const Position& i_pos)
{
    assert(i_pos.getLigne()>-1);
    assert(i_pos.getLigne()<NB_LIGNES);
    assert(i_pos.getColonne()>-1);
    assert(i_pos.getColonne()<NB_COLONNES);

    return m_cases[i_pos.getLigne()][i_pos.getColonne()];
};

/*
 Description: accesseur en lecture du contenu de la grille par une position
 param[E] i_pos : une position
 pre i_pos doit être à l'intérieur de la grille
 retour une case de la grille
 post une référence  constante sur la case pour qu'elle ne puisse pas être modifiée
 precision la méthode est valide avec les assertions
*/


const Grille::Case& Grille::operator[](const Position& i_pos) const
{
    assert(i_pos.getLigne()>-1);
    assert(i_pos.getLigne()<NB_LIGNES);
    assert(i_pos.getColonne()>-1);
    assert(i_pos.getColonne()<NB_COLONNES);

    return m_cases[i_pos.getLigne()][i_pos.getColonne()];
};

/*
 Description: accesseur du nombre de lignes de la grille
 retour entier
 post la méthode retourne le nombre de lignes (constante) de la grille
*/

int Grille::getNbLignes() {
    return NB_LIGNES;
};

/*
 Description: accesseur du nombre de colonnes de la grille
 retour entier
 post la méthode retourne le nombre de colonnes (constante) de la grille
*/


int Grille::getNbColonnes() {
    return NB_COLONNES;
};

/*
 Description: surcharge de l'opérateur<<
 param[E/S] out : flux de sortie
 param[E] i_grille : grille à afficher sur le flux de sortie
 retour flux de sortie
 post l'opérateur affiche une grille, soit de bateaux (nos), soit de tirs ('0' ratés, '*' réussis) avec entête et no. de lignes et colonnes, des tirets pis toute pis toute
*/

ostream& operator<<(ostream& out, const Grille& i_grille)
{
    // on définit des constantes pour l'affichage seulement
    const char EAU = ' ';
    const char TIR = '0';
    const char TOUCHE = '*';

    out << "    ";
    for (char col = 'A'; col < 'A'+Grille::NB_COLONNES; ++col)
    {
        out << col << ' ';
    }

    out << endl << "----";
    for (int i = 0; i < Grille::NB_COLONNES; ++i)
    {
        out << "--";
    }
    out << endl;
    for (int i = 0; i < Grille::NB_LIGNES; ++i)
    {
        out << i + 1;
        if ((i + 1) < 10)
            out << ' ';
        out << "| ";
        for (int j = 0; j < Grille::NB_COLONNES; ++j)
        {
            if (i_grille.m_cases[i][j] == Grille::Case::EAU)
                out << EAU << ' ';
            else if (i_grille.m_cases[i][j] == Grille::Case::TIR_RATE)
                out << TIR << ' ';
            else if (i_grille.m_cases[i][j] == Grille::Case::TOUCHE)
                out << TOUCHE << ' ';
            else
                out << (int) i_grille.m_cases[i][j] << ' ';
        }
        out << endl;
    }

    out << "----";
    for (int i = 0; i < Grille::NB_COLONNES; ++i)
    {
        out << "--";
    }
    out << endl;

    return out;
}
