#ifndef JEU_H
#define JEU_H

#include "joueur.h"
#include "joueurIA.h"

class BatailleNavale
{
public:
    BatailleNavale();
   
    /*
    *  Description: Effectue la boucle de jeu
    */
    void jouer();
    
private:
    Joueur      m_joueur;
    JoueurIA    m_ordinateur;

    /*
     Description: joue un tour du jeu (joueur et ordinateur ensuite)
     param[E] i_cCol : Caractere de la colonne d'attaque
     param[E] i_nLig : Numero de la ligne d'attaque
     retour faux si la partie est terminée et vrai si elle doit continuer
    */

    bool jouerUnTour(char, int);

};

#endif
