/*
* fichier : jeu.cpp
* auteur.e : Vincent Ducharme
* date : 2016
* modifications :
* 01/11/2019 Marie-Flavie Auclair-Fortier : refactoring du code
* description : Ce fichier contient un jeu de bataille navale
*/

#include "jeu.h"

#include <iostream>

using namespace std;

/*
*  Description: Constructeur
*/

BatailleNavale::BatailleNavale() :
	m_joueur{},
	m_ordinateur{}
{}

/*
*  Description: Effectue la boucle de jeu
*/

void BatailleNavale::jouer()
{
	// Fonctions locales
	bool positionEstValide(char, int);

	// Variables locales
	char    cColonne;
	int     nLigne;

	cout << "Vos tirs et vos bateaux" << endl;
	m_joueur.afficherTirs();
	m_joueur.afficherBateaux();

	// enlever les 3 lignes de codes pour jouer pour de vrai...
	cout << "Tirs et bateaux de l'ordinateur" << endl;
	m_ordinateur.afficherTirs();
	m_ordinateur.afficherBateaux();

	cout << "=================================================" << endl;
	cout << "Entrez la position colonne (q pour quitter) : ";
	cin >> cColonne;

	while (cColonne != 'q')
	{
		cout << "Entrez la position ligne : ";
		cin >> nLigne;
		cout << endl << "=================================================" << endl;

		// On s'assure que les positions d'attaque demandees sont dans la grille
		if (positionEstValide(cColonne, nLigne))
		{
			if (!jouerUnTour(cColonne, nLigne)) return;
		}
		else
		{
			cout << "/////////////////////////////////////////" << endl;
			cout << "Position invalide! Reessayez!" << endl;
			cout << "/////////////////////////////////////////" << endl << endl;
		}

		cout << "vos tirs" << endl;
		m_joueur.afficherTirs();

		cout << "tirs de l'ordinateur" << endl;
		m_ordinateur.afficherTirs();

		cout << "vos bateaux" << endl;
		m_joueur.afficherBateaux();

		cout << "=================================================" << endl;
		cout << "Entrez la position colonne, (q pour quitter) : ";
		cin >> cColonne;
	}

	return;
}

/*
 Description: Valide si la position est a l'interieur de la grille de jeu
 param[E] i_cCol : Caractere de la colonne d'attaque
 param[E] i_nLig : Numero de la ligne d'attaque
 retour vrai si la position est bien dans la grille
*/

bool positionEstValide(char i_cCol, int i_nLig)
{
	return (i_cCol >= 'A') && (i_cCol < 'A' + Grille::getNbColonnes())
		&& (i_nLig >= 1) && (i_nLig <= Grille::getNbLignes());
}

/*
 Description: joue un tour du jeu (joueur et ordinateur ensuite)
 param[E] i_cCol : Caractere de la colonne d'attaque
 param[E] i_nLig : Numero de la ligne d'attaque
 retour faux si la partie est terminée et vrai si elle doit continuer
*/

bool BatailleNavale::jouerUnTour(char cColonne, int nLigne)
{
	Position posJoueur{ nLigne - 1, cColonne - 'A' };

	// On verifie si l'attaque est valide (pas deja tiree)
	if (m_joueur.validerTir(posJoueur))
	{
		// Tour du joueur
		bool nCoule;
		// Verifie si l'attaque touche un bateau de l'adversaire
		if (m_ordinateur.validerAttaqueOpposant(posJoueur, nCoule))
		{
			// Si oui, on effectue une touche
			m_joueur.toucher(posJoueur);

			// Verifie si l'ordinateur a perdu
			if (m_ordinateur.validerSiPerdu())
			{
				// Si oui, on a gagne, la partie arrête
				cout << "L'ennemi est defait! Vous avez gagne!" << endl << endl << endl;
				return false;
			}
		}
		else
		{
			// Si non, on a effectue un tir a l'eau
			m_joueur.rater(posJoueur);
		}

		// Tour de l'ordinateur
		cout << "L'ordi joue" << endl;
		Position posIA;

		// Genere l'attaque du joueur
		posIA = m_ordinateur.genererAttaque();

		// Verifie si l'attaque de l'IA touche un bateau du joueur
		if (m_joueur.validerAttaqueOpposant(posIA, nCoule))
		{
			// Si oui, on effectue une touche
			m_ordinateur.toucher(posIA, nCoule);

			// Si le joueur a perdu
			if (m_joueur.validerSiPerdu())
			{
				// L'ordinateur a gagne, la partie arrête
				cout << "Vous n'avez plus de bateaux. Vous avez perdu!" << endl << endl << endl;
				return false;
			}
		}
		else
		{
			// Si non, on effectue un tir a l'eau
			m_ordinateur.rater(posIA);
		}
	}
	else
	{
		cout << "/////////////////////////////////////////" << endl;
		cout << "Vous avez deja tire sur cette position!" << endl;
		cout << "Reessayez!" << endl;
		cout << "/////////////////////////////////////////" << endl << endl;
	}

	// la partie continue
	return true;
}
