/*
* fichier : main.cpp
* auteur.e : Vincent Ducharme
* date : 2016
* modifications :
* 01/11/2019 Marie-Flavie Auclair-Fortier : refactoring du code
* description : Ce fichier contient un jeu de bataille navale
*/

#include "jeu.h"

#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    ifstream fichierTest;   // flux d'entree de type fichier

    // workaround pour avoir une entree redirigee dans le fichier de test
    fichierTest.open("test");  // ouvre le fichier
    
    // redirige le flux standard vers le fichier, le cin pourra etre utilise normalement
    // ****** a mettre en commentaires cette ligne pour remettre le fonctionnement a normal
//    cin.rdbuf(fichierTest.rdbuf());

    BatailleNavale jeu;

    jeu.jouer();

    return 0;
}
