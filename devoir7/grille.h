//
//  grille.h
//  D7-BatailleNavale
//
//  Created by Marie-Flavie Auclair-Fortier on 18-03-21.
//  Copyright © 2018 Marie-Flavie Auclair-Fortier. All rights reserved.
//

#ifndef GRILLE_H
#define GRILLE_H

#include "position.h"

#include <array>
#include <iostream>

using namespace std;

class Grille
{
public:

    enum Case
    {
        EAU = 0,
        TORPILLEUR = 1,
        SOUS_MARIN = 2,
        CONTRE_TORPILLEUR = 3,
        CROISEUR = 4,
        PORTE_AVION = 5,
        TIR_RATE = 6,
        TOUCHE = 7        
    };

    /*
     Description: constructeur par défaut
     post tous les éléments de la grille sont initialisés à EAU
    */

    Grille();
    
    /*
     Description: accesseur en écriture du contenu de la grille par une position
     param[E] i_pos : une position
     pre i_pos doit être à l'intérieur de la grille
     retour une case de la grille
     post une référence non constante sur la case pour qu'elle puisse être modifiée
     precision la méthode est valide avec les assertions
    */

    Case& operator[](const Position&);
    
    /*
     Description: accesseur en lecture du contenu de la grille par une position
     param[E] i_pos : une position
     pre i_pos doit être à l'intérieur de la grille
     retour une case de la grille
     post une référence  constante sur la case pour qu'elle ne puisse pas être modifiée
     precision la méthode est valide avec les assertions
    */

    const Case& operator[](const Position&) const;

    /*
     Description: accesseur du nombre de lignes et de colonnes de la grille
     retour entier
     post retourne le nombre de lignes ou de colonnes (constante) de la grille
    */

    static int getNbLignes();
    static int getNbColonnes();

    /*
     Description: surcharge de l'opérateur<<
     param[E/S] out : flux de sortie
     param[E] i_grille : grille à afficher sur le flux de sortie
     retour flux de sortie
     post l'opérateur affiche une grille, soit de bateaux (nos), soit de tirs ('0' ratés, '*' réussis) avec entête et no. de lignes et colonnes, des tirets pis toute pis toute
    */


    friend ostream& operator<<(ostream&, const Grille&);

private:
   
    static const int NB_COLONNES = 10;
    static const int NB_LIGNES = 10;
    
    // Tableau 2D de Cases
    array<array<Case, NB_COLONNES>, NB_LIGNES> m_cases;
};
#endif
