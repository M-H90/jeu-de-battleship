#include "grille.h"

Position::Position()
{
	_ligne = 0;
	_colonne = 0;
}

Position::Position(int ligne, int colonne)
{
	_ligne = ligne;
	_colonne = colonne;
}

int Position::getLigne() const
{
	return _ligne;
}

int Position::getColonne() const
{
	return _colonne;
}

Position Position::voisinDroite() const
{
	int nCol = _colonne + 1;
	Position nPos{ _ligne, nCol };
	if (nCol > Grille::getNbColonnes())
	{
		nPos = INVALIDE;
	}
	return nPos;
}

Position Position::voisinGauche() const
{
	Position pos = { _ligne, _colonne - 1 };
	if (pos < Position{ 0, 0 })
	{
		pos = INVALIDE;
	}
	return pos;
}

Position Position::voisinHaut() const
{
	Position pos = { _ligne - 1, _colonne };
	if (pos < Position{ 0, 0 })
	{
		pos = INVALIDE;
	}
	return pos;
}

Position Position::voisinBas() const
{
	int nLig = _ligne + 1;
	Position nPos{ nLig, _colonne };
	if (nLig > Grille::getNbLignes())
	{
		nPos = INVALIDE;
	}
	return nPos;
}

bool Position::operator == (Position posi) const
{
	if (_ligne == posi._ligne || _colonne == posi._colonne)
	{
		return true;
	}

	return false;
}

bool Position::operator < (Position posi) const
{
	if (_ligne < posi._ligne || _colonne < posi._colonne)
	{
		return true;
	}

	return false;
}

