#include "joueurIA.h"
#include "constantes.h"
#include "bnRandom.h"

JoueurIA::JoueurIA()
{
	Joueur::Joueur();
}


void JoueurIA::afficherTirs() const
{
	cout << m_tirs;
}

void JoueurIA::afficherBateaux() const
{
	Joueur::afficherBateaux();
}

bool JoueurIA::validerTir(const Position& i_pos) const
{
	return Joueur::validerTir(i_pos);
}

bool JoueurIA::validerAttaqueOpposant(const Position& i_pos, bool& o_estCoule)
{
	return Joueur::validerAttaqueOpposant(i_pos, o_estCoule);
}

bool JoueurIA::validerSiPerdu() const
{
	return Joueur::validerSiPerdu();
}

bool JoueurIA::validerSiCoule(const Position& i_pos) const
{
	return Joueur::validerSiCoule(i_pos);
}

Position JoueurIA::genererAttaque()
{
	Position i_pos;

	if (dernPosValide.size() >= 2)
	{
		if (dernPosValide[0].getLigne() + 1 == dernPosValide[1].getLigne() ||
			dernPosValide[0].getLigne() - 1 == dernPosValide[1].getLigne())
		{
			for (int i = dernPosValide.size() - 1; i >= 0; i--)
			{
				if (isValid(dernPosValide[i].voisinHaut()) && m_tirs[dernPosValide[i].voisinHaut()] == Grille::EAU)
				{
					return dernPosValide[i].voisinHaut();
				}
				else if (isValid(dernPosValide[i].voisinBas()) && m_tirs[dernPosValide[i].voisinBas()] == Grille::EAU)
				{
					return dernPosValide[i].voisinBas();
				}
			}
		}
		else if (dernPosValide[0].getColonne() + 1 == dernPosValide[1].getColonne() ||
			dernPosValide[0].getColonne() - 1 == dernPosValide[1].getColonne())
		{
			for (int i = dernPosValide.size() - 1; i >= 0; i--)
			{
				if (isValid(dernPosValide[i].voisinDroite()) && m_tirs[dernPosValide[i].voisinDroite()] == Grille::EAU)
				{
					return dernPosValide[i].voisinDroite();
				}
				else if (isValid(dernPosValide[i].voisinGauche()) && m_tirs[dernPosValide[i].voisinGauche()] == Grille::EAU)
				{
					return dernPosValide[i].voisinGauche();
				}
			}
		}
	}

	if (dernPosValide.size() != 0)
	{
		for (int i = dernPosValide.size() - 1; i >= 0; i--)
		{
			if (m_tirs[dernPosValide[i].voisinDroite()] == Grille::EAU)
			{
				return dernPosValide[i].voisinDroite();
			}
			else if (m_tirs[dernPosValide[i].voisinGauche()] == Grille::EAU)
			{
				return dernPosValide[i].voisinGauche();
			}
			else if (m_tirs[dernPosValide[i].voisinHaut()] == Grille::EAU)
			{
				return dernPosValide[i].voisinHaut();
			}
			else if (m_tirs[dernPosValide[i].voisinBas()] == Grille::EAU)
			{
				return dernPosValide[i].voisinBas();
			}
		}
	}
	else
	{
		int posC = genereValeur(0, Grille::getNbColonnes() - 1);
		int posL = genereValeur(0, Grille::getNbLignes() - 1);

		i_pos = Position{ posL, posC };

		if (m_tirs[i_pos] != Grille::EAU)
		{
			i_pos = genererAttaque();
		}
	}

	return i_pos;
}

void JoueurIA::toucher(const Position& i_pos, bool nCoule)
{
	m_tirs[i_pos] = Grille::TOUCHE;

	dernPosValide.push_back(i_pos);

	if (nCoule)
	{
		dernPosValide.clear();
	}

}

void JoueurIA::rater(const Position& i_pos)
{
	m_tirs[i_pos] = Grille::TIR_RATE;
}

bool JoueurIA::isValid(const Position& i_pos)
{
	if (i_pos.getLigne() > -1 && i_pos.getLigne() < Grille::getNbLignes() &&
		i_pos.getColonne() > -1 && i_pos.getColonne() < Grille::getNbColonnes())
	{
		return true;
	}
	return false;
}
