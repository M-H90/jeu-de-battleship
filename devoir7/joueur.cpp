/*
* fichier : jeu.cpp
* auteur.e : Vincent Ducharme
* date : 2016
* modifications :
* 01/11/2019 Marie-Flavie Auclair-Fortier : refactoring du code
* description : Ce fichier contient la logique du joueur
*/

#include "joueur.h"
#include "constantes.h"
#include "bnRandom.h"

#include <iostream>

using namespace std;

/*
 Description: constructeur par defaut
*/

Joueur::Joueur() :
    m_tirs{} ,
    m_bateaux{},
    m_tirsEnnemi{}
{
    placerBateaux();
}

/*
 Description: affiche le tableau des tirs
 post affichage de la grille des tirs avec entete et no. des lignes, '0' pour un tir rate, '*' pour un tir reussi
*/

void Joueur::afficherTirs() const
{
    cout << m_tirs;
}

/*
 Description: affiche le tableau des tirs de l'ennemi
 post affichage de la grille des bateaux avec entete et no. des lignes, no. des bateaux a chaque emplacement et '*' pour un tir réussi
*/

void Joueur::afficherBateaux() const
{
    cout << m_tirsEnnemi;
}

/*
 Description: valide si un tir a deja ete effectue a une position donnee
 param[E] i_pos : position a verifier
 pre i_pos doit etre a l'interieur des bornes de la grille
 retour booleen
 post vrai si aucun tir n'a deja ete effectue (contenu de la grille est EAU)
    faux sinon ou si la position est INVALID
*/

bool Joueur::validerTir(const Position& i_pos) const
{
    if (i_pos == INVALIDE) {
        return false;
    }
    
    assert(i_pos.getLigne()>-1);
    assert(i_pos.getLigne()<Grille::getNbLignes());
    assert(i_pos.getColonne()>-1);
    assert(i_pos.getColonne()<Grille::getNbColonnes());

    return m_tirs[i_pos] == Grille::EAU;
}

/*
 Description: affecte le contenu de la case des tirs a touché
 param[E] i_pos : position du tir
 pre i_pos doit etre a l'interieur des bornes de la grille
 post la grille de tirs a la position contient la constante TOUCHE
*/

void Joueur::toucher(const Position& i_pos)
{
    assert(i_pos.getLigne()>-1);
    assert(i_pos.getLigne()<Grille::getNbLignes());
    assert(i_pos.getColonne()>-1);
    assert(i_pos.getColonne()<Grille::getNbColonnes());

    m_tirs[i_pos] = Grille::TOUCHE;
}

/*
 Description: affecte le contenu de la case des tirs a raté
 param[E] i_pos : position du tir
 pre i_pos doit etre a l'interieur des bornes de la grille
 post la grille de tirs a la position contient la constante TIR_RATE
*/

void Joueur::rater(const Position& i_pos)
{
    assert(i_pos.getLigne()>-1);
    assert(i_pos.getLigne()<Grille::getNbLignes());
    assert(i_pos.getColonne()>-1);
    assert(i_pos.getColonne()<Grille::getNbColonnes());

    m_tirs[i_pos] = Grille::TIR_RATE;
}

/*
 Description: valide si le joueur a perdu
 retour booléen
 post vrai si le joueur a perdu (si toutes les cases sont soit à EAU ou TOUCHE)
    faux sinon
*/

bool Joueur::validerSiPerdu() const
{
    for (int i = 0; i < Grille::getNbLignes(); ++i)
    {
        for (int j = 0; j < Grille::getNbColonnes(); ++j)
        {
            if (m_tirsEnnemi[{i, j}] != Grille::EAU && m_tirsEnnemi[{i, j}] != Grille::TOUCHE)
                return false;
        }
    }

    return true;
}

/*
 Description: valide si l'attaque de l'opposant a touché quelquechose
 param[E] i_pos : position a verifier
 pre i_pos doit etre a l'interieur des bornes de la grille
 param[S] o_coule : bouléen, indique si le bateau est coulé
 retour booléen
 post la fonction retourne vrai si le bateau est touché, faux sinon
        le paremètre en sortie est vrai si le bateau est coulé, faux sinon
*/

bool Joueur::validerAttaqueOpposant(const Position& i_pos, bool& o_estCoule)
{
    assert(i_pos.getLigne()>-1);
    assert(i_pos.getLigne()<Grille::getNbLignes());
    assert(i_pos.getColonne()>-1);
    assert(i_pos.getColonne()<Grille::getNbColonnes());

    o_estCoule = false;
    
    if (m_bateaux[i_pos] != Grille::EAU)
    {
        cout << "************************" << endl;
        cout << "Touche!!" << endl;

        m_tirsEnnemi[i_pos] = Grille::TOUCHE;

        if (validerSiCoule(i_pos))
        {
            cout << "Coule!!" << endl;
            o_estCoule = true;
        }

        cout << "************************" << endl << endl;
        return true;
    }
    else
    {
        cout << "++++++++++++++++++++++++" << endl;
        cout << "A l'eau!!" << endl;
        cout << "++++++++++++++++++++++++" << endl << endl;
        
        return false;
    }
}

/*
 Description: valide si le bateau est coulé à partir d'un touche
 param[E] i_pos : position a verifier
 pre i_pos doit etre a l'interieur des bornes de la grille
    le contenu de la grille de tirs à i_pos devrait être TOUCHE
 retour booléen
 post retourne vrai si toutes les cases d'un même bateau ont été touchées
  faux sinon
*/

bool Joueur::validerSiCoule(const Position& i_pos) const
{
    assert(i_pos.getLigne()>-1);
    assert(i_pos.getLigne()<Grille::getNbLignes());
    assert(i_pos.getColonne()>-1);
    assert(i_pos.getColonne()<Grille::getNbColonnes());
    assert(m_tirsEnnemi[i_pos]==Grille::TOUCHE);

    Position pos{i_pos};
    
    Grille::Case idBateau {m_bateaux[i_pos]};

    bool estHorizontal {false};
    
    // déterminer dans quel sens est le bateau
    if (i_pos.getColonne() == 0)
        estHorizontal = (m_bateaux[i_pos.voisinDroite()] == idBateau);
    else if (i_pos.getColonne() == Grille::getNbColonnes() - 1)
        estHorizontal = (m_bateaux[i_pos.voisinGauche()] == idBateau);
    else
        estHorizontal = (m_bateaux[i_pos.voisinDroite()] == idBateau ||
                       m_bateaux[i_pos.voisinGauche()] == idBateau);

    bool estCoule{true};

    if (estHorizontal)
    {
        // Trouver le début du bateau
        while ((pos.getColonne() > 0) && (m_bateaux[pos.voisinGauche()] == idBateau))
            pos = pos.voisinGauche();
        
        // Vérifier toutes les cases du bateau
        for (int i = 0; i < TAILLE_BATEAUX[idBateau]; ++i){
            estCoule = (estCoule && m_tirsEnnemi[pos] == Grille::TOUCHE);
            pos = pos.voisinDroite();
        }
    }
    else
    {
        // Trouver le début du bateau
        while (pos.getLigne() > 0 && m_bateaux[pos.voisinHaut()] == idBateau)
            pos = pos.voisinHaut();
      
        // Vérifier toutes les cases du bateau
        for (int i = 0; i < TAILLE_BATEAUX[idBateau]; ++i){
             estCoule = (estCoule && m_tirsEnnemi[pos] == Grille::TOUCHE);
             pos = pos.voisinBas();
        }
    }
    
    return estCoule;
}

/*
 Description: Valide si un bateau entre dans la grille à partir d'une position
 param[E] i_pos : position a verifier
            i_taille : taille du bateau
            i_orientation : orientation du bateau (HORIZONTAL ou non)
 pre i_pos doit etre a l'interieur des bornes de la grille
    i_taille est entre 2 et 5
 retour booléen
 post vrai si le bateau entre dans la grille et il n'y a pas de collision
    faux sinon
 precision la méthode est valide avec les assertions
*/

bool Joueur::validerPositionBateau(const Position& i_pos, int i_taille, bool i_orientation) const
{
    assert(i_pos.getLigne()>-1);
    assert(i_pos.getLigne()<Grille::getNbLignes());
    assert(i_pos.getColonne()>-1);
    assert(i_pos.getColonne()<Grille::getNbColonnes());
    assert(i_taille>1);
    assert(i_taille<6);

    Position pos{i_pos};

    // déterminer si le bateau entre dans la grille à partir de cette position
    if (i_orientation == HORIZONTAL)
    {
        if (i_pos.getColonne() + i_taille > Grille::getNbColonnes() - 1)
            return false;
    }
    else
    {
        if (i_pos.getLigne() + i_taille > Grille::getNbLignes() - 1)
            return false;
    }
    
    bool valide {m_bateaux[pos] == Grille::Case::EAU};
    
    // déterminer s'il y a collision avec un autre bateau
    for (int i = 1; i < i_taille; ++i)
    {
        if (i_orientation == HORIZONTAL)
            pos = pos.voisinDroite();
        else
            pos = pos.voisinBas();
        
        valide = valide && (m_bateaux[pos] == Grille::Case::EAU);
        
    }
    
    return valide;
}

/*
 Description: préparer le jeu en placant les bateaux du joueur
 post les 5 bateaux placés au hasard sans collision et à l'intérieur de la grille des bateaux. La grille des tirs ennemis est aussi initialisés avec les informations de bateaux.
*/

void Joueur::placerBateaux()
{
    int noBateau {Grille::Case::PORTE_AVION};
    
    // on boucle sur les bateaux (en partant du dernier qui sera plus facile a entrer ainsi)
    while (noBateau > Grille::Case::EAU)
    {
        bool estHorizontal;
        Position pos;

        // on genere une position et une orientation jusqu'a ce que le bateau entre
        do
        {
            pos = {genereValeur(0, Grille::getNbLignes() - 1),
                   genereValeur(0, (Grille::getNbColonnes() - 1))};
            
            estHorizontal = genereValeur(VERTICAL, HORIZONTAL);
            
        } while (!validerPositionBateau(pos, TAILLE_BATEAUX[noBateau], estHorizontal));

        // premiere case du bateau
        m_bateaux[pos] = (Grille::Case) noBateau;
        
        // les autres cases du bateau dépendement de l'orientation
        if (estHorizontal)
        {
            for (int i = 1; i < TAILLE_BATEAUX[noBateau]; ++i){
                pos = pos.voisinDroite();
                m_bateaux[pos] = (Grille::Case) noBateau;
            }
        }
        else
        {
            for (int i = 1; i < TAILLE_BATEAUX[noBateau]; ++i){
                pos = pos.voisinBas();
                m_bateaux[pos] = (Grille::Case) noBateau;
            }
        }

        noBateau--;
    }

    // on copie les informations dans la grille des tirs de l'ennemi pour ne pas contaminer notre grille de bateaux
    m_tirsEnnemi = m_bateaux;

}
